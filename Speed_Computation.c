#include <hidef.h> /* common defines and macros */
#include <mc9s12dg256.h> /* derivative information */
#pragma LINK_INFO DERIVATIVE "mc9s12dg256b"
#include "main_asm.h" /* interface to the assembly module */


//char digits[4];
int Counter1=0;
int RTI_Counter=0;
int speed=0;

void Check_speed()
{
	RTI_disable();
    IRQ_disable();	
    speed = Counter1/2; 
    
    //Calculates which RGB should be turned on
    if(speed < 300){
        PTP = PTP | 0x20;
    }else{
        PTP = PTP & 0xDF;
    }
    if(speed > 3000){
        PTP = PTP | 0x10;
    }else{
        PTP = PTP & 0xEF; 
    }
    if(speed > 300 && speed < 3000){
        PTP = PTP | 0x40;
    }else{
        PTP = PTP & 0xBF;
    }
                      
    //Puts value to 7-segment
    PTP = PTP | 0b00000001;
    PTP = PTP & 0b11111000;
    PORTB = speed/1000;
    PTP = PTP & 0b11110100;
    PTP = PTP | 0b00000010;
    while(speed > 1000){
        speed = speed-1000;
    }
    PORTB = speed/100;
    PTP = PTP & 0b11110010;
    PTP = PTP | 0b00000100; 
    while(speed > 100){
        speed = speed-100;
    }
    PORTB = speed/10;
    PTP = PTP & 0b11110001;
    PTP = PTP | 0b00001000; 
    while(speed > 10){
        speed = speed-10;
    } 
    PORTB = speed;
	
  	Counter1=0;
    RTI_Counter=0;
    
    RTI_enable();
	IRQ_enable()
} 

// RTI 
void interrupt 7 handler() {

clear_RTI_flag();
RTI_Counter++;
  if(RTI_Counter == 480){
   Check_speed();
  }        
 }


  // IRQ 
  
void interrupt 6 handler1() {
 Counter1++; 
 }

 

void main (){
     

      PWM_Init(120,0,0,60); // set system clock frequency to 24 MHz
      RTI_init();
      RTI_enable();
      IRQ_enable();
      lcd_init();
      seg7_enable();
      SW_enable();
      DDRP = DDRP 0x70
      _asm(cli); //enable interrupts
         
      RTICTL=0x7F;
      INTCR=0xC0;
      DDRJ = 0xFF;
      DDRB = 0xFF; //set port b to output 
      DDRH = 0x00;//set port H as input
      DDRP = 0xFF; //set port P as output
      
     while(1){
   
     }
      
  
}